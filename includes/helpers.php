<?php

//* Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

/*
 *
 */
function phut_kb_summary() { ?>
<li><a href="<?php the_permalink() ?>" title="<?php the_title_attribute() ?>"><?php the_title() ?></a></li>
<?php } //fn
