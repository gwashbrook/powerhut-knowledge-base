<?php
//* Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

define( 'PHUT_KB_ENABLE_SEARCH', false );

define( 'PHUT_KB_ENABLE_VOTING', false );

// Set Knowledge base archive as home page
define( 'PHUT_KB_HOMEPAGE', true );

// Number of articles per archive page
define('PHUT_KB_PER_PAGE', 24);
