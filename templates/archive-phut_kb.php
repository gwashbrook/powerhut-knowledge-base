<?php 

// Some Custom CSS
add_action ('wp_head','phut_kb_css');
function phut_kb_css(){
?>
<style>

	ul.phut-kb-categories,
	ul.phut-kb-archive {
		margin: 0 0 1.5em 0 !important;
	}


	ul.phut-kb-categories li,
	ul.phut-kb-archive li {
		margin-bottom: 16px;
	}

	ul.phut-kb-categories li {
		float: left;
		margin-left: 2.5641025641%;
		text-align: center;
		width: 31.6239316239%; /* one-third */
	}

	ul.phut-kb-categories li:nth-child(3n+1){ /* 3 columns */
		clear:left;
		margin-left: 0;
	}


	ul.phut-kb-categories li a,
	ul.phut-kb-archive li a {
		background-color: #FFF;
		border: 1px solid #F5E1C5;
		display: block;
		text-decoration: none;
	}
	
	ul.phut-kb-categories li a {
		min-height: 80px;
		font-size: 24px;
		font-size: 2.4rem;
		padding: 24px 16px;
	}
	
	ul.phut-kb-archive li a {
		padding: 16px;
	}	


	ul.phut-kb-archive li a:before {
		font-family: FontAwesome;
		content: "\f0f6";
		display: inline-block;
		margin-right: .5em;
		color: #5d4037;
	}


	ul.phut-kb-categories li a:hover,
	ul.phut-kb-categories li a:focus,
	ul.phut-kb-categories li a:active,
	ul.phut-kb-archive li a:hover,
	ul.phut-kb-archive li a:focus,
	ul.phut-kb-archive li a:active {
	
		border-color: #ff5722;
	}


	



</style>
<?php }





$id = get_queried_object_id();

// Default args for wp_list_categories
$args = array (
	'taxonomy'   => 'phut_kb_category',
	'echo'       => 0,
	// 'orderby' => 'name', // default 'name'
	// 'order' => 'ASC', // default 'ASC'
	// 'hide_empty' => 0, // default 1 (Hide if empty)
	'hide_title_if_empty' => true,
	// 'depth'      => 1,
	'show_count' => 0,
	'title_li' => 0,
	// 'style' => '', // default 'list'
	// 'depth' => 1,
	'show_option_none' => '',
);

// Remove the Genesis loop
remove_action( 'genesis_loop', 'genesis_do_loop' );

// Replaces the Genesis loop
function phut_kb_archive_loop(){
	if ( have_posts() ) :

		echo '<ul class="phut-kb-archive">';
		// do_action( 'genesis_before_while' );
		while ( have_posts() ) : the_post();

		  // do_action( 'genesis_before_entry' );

		// genesis_markup( array(
			// 'open'    => '<article %s>',
			// 'context' => 'entry',
		// ) );

		// do_action( 'genesis_entry_header' );
		
		
		phut_kb_summary();
		
		

		// do_action( 'genesis_before_entry_content' );

		// printf( '<div %s>', genesis_attr( 'entry-content' ) );
		// do_action( 'genesis_entry_content' );
		// echo '</div>';

		// do_action( 'genesis_after_entry_content' );

		// 	do_action( 'genesis_entry_footer' );
		
		// genesis_markup( array(
			// 'close'   => '</article>',
			// 'context' => 'entry',
		 //  ) );

		// do_action( 'genesis_after_entry' );

		endwhile;
		echo '</ul>';
		
		do_action( 'genesis_after_endwhile' );
		

	  else : // If no posts exist.
		do_action( 'genesis_loop_else' );
	  endif; // End loop.

}


if( 0 == $id ) {

	// We are on the main archive view, so just show the main categories as links
	$args['depth'] = 1;

} else {

	// We have a term id
	$this_term = get_term( $id );
	
	// Does this term have a parent?
	if( $this_term->parent ) {
		
		// List Sibling Categories
		$args['child_of'] = $this_term->parent;
		$args['exclude'] = $id;

	} else {

		// List Children Categories
		$args['child_of'] = $id;	
	}

	add_action( 'genesis_loop', 'phut_kb_archive_loop' );
	


}



add_action ( 'genesis_before_loop', 'phut_kb_search');

add_action( 'genesis_before_loop', function() use( $args ) {

	$categories = wp_list_categories( $args );

	if( $categories )
		printf( '<ul class="phut-kb-categories clearfix">%s</ul>', $categories );
		
		
} );




/*
    if (is_category()) {
    	$this_category = get_category($cat);
    }

	if($this_category->category_parent)
		$this_category = wp_list_categories(
			'orderby=id
			&show_count=0
			&title_li=
			&use_desc_for_title=1
			&child_of='.$this_category->category_parent.
    "&echo=0"); else
    $this_category = wp_list_categories('orderby=id&depth=1&show_count=0
    &title_li=&use_desc_for_title=1&child_of='.$this_category->cat_ID.
    "&echo=0");
    if ($this_category) { ?> 
 
<ul>
<?php echo $this_category; ?>
 
</ul>
 
<?php } ?>


*/

// Get a list of all kb categories













// wp_list_categories( $args );









genesis();