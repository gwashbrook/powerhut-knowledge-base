<?php
/*
Plugin Name: Powerhut Knowledge Base
Description: Plugin description goes here
Plugin URI: https://powerhut.net/
Author: Graham Washbrook
Author URI: https://powerhut.tel
Text Domain: plugin-text-domain
Domain Path: /languages
Version: 0.0.3
*/

//* Search and Replace Strings
// namespace = ns_ ~ e.g. phut_kb_
// defined namespace path = NS_PATH ~ e.g. PHUT_KB_PATH
// custom post type name = cpt_name ~ e.g. phut_kb
// image size(s) = cpt-name-archive ~ e.g. phut-kb-archive
// custom post type slug = cpt-slug ~ e.g. knowledgebase
// custom post type plural label = CPT_PLURAL_LABEL ~ e.g. Articles
// custom post type singular label = CPT_SINGLE_LABEL ~ e.g. Article
// custom tag name = tag_name ~ e.g. phut_
// custom tag plural label (lower case) = TAG_PLURAL_LC_LABEL ~ e.g. timelines
// custom tag plural label = TAG_PLURAL_LABEL ~ e.g. Timelines
// custom tag singular label = TAG_SINGLE_LABEL ~ e.g. Timeline

//* Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

//* Define plugin path
define( 'PHUT_KB_PATH', plugin_dir_path( __FILE__ ) );

//* Includes and requires
require( PHUT_KB_PATH . 'config.php');
require( PHUT_KB_PATH . 'includes/helpers.php');

//* On plugin activation
function phut_kb_activation() {
	if ( ! current_user_can( 'activate_plugins' ) )  return;
	phut_kb_init();
	flush_rewrite_rules();
}
register_activation_hook( __FILE__, 'phut_kb_activation' );

//* On plugin deactivation
function phut_kb_deactivation() {
	flush_rewrite_rules();
}
register_deactivation_hook( __FILE__, 'phut_kb_deactivation' );

//* On plugin uninstall
function phut_kb_uninstall() {
	/*
	// e.g. delete options
	$option_name = 'phut_kb_option';
 	delete_option($option_name);
 
	// for site options in Multisite
	delete_site_option($option_name);
 
	// eg. drop a custom database table
	global $wpdb;
	$wpdb->query("DROP TABLE IF EXISTS {$wpdb->prefix}mytable");
	*/	
}
register_uninstall_hook( __FILE__, 'phut_kb_uninstall');

//* Init
function phut_kb_init() {

	// Register taxonomies
	phut_kb_register_taxonomies();

	// Register post types
	phut_kb_register_cpt();

	// Better be safe than sorry when registering custom taxonomies for custom post types.
	// register_taxonomy_for_object_type( 'phut_kb_tag', 'phut_kb' );
	register_taxonomy_for_object_type( 'phut_kb_category', 'phut_kb' );

	// Change enter title here text
	add_filter( 'enter_title_here', 'phut_kb_enter_title_here' );

	// Change the post updated messages
	add_filter( 'post_updated_messages', 'phut_kb_updated_messages' );

	// Change the bulk post updated messages
	add_filter( 'bulk_post_updated_messages', 'phut_kb_bulk_updated_messages' );

	// Add image size
	// add_image_size( 'phut-kb-archive', 370, 370, array( 'left', 'top' ) );
	
} //fn
add_action( 'init', 'phut_kb_init', 0 );

//* Register taxonomies
function phut_kb_register_taxonomies() {

	// TAG
	
	$labels = array(

		'name'          => _x( 'Knowledge Base Tags', 'taxonomy general name' ),
		'singular_name' => _x( 'Tag', 'taxonomy singular name' ),
		'menu_name'     => _x( 'Tags', 'taxonomy general name' ),
		'all_items'     => __( 'All Tags', 'plugin-text-domain' ),
		'edit_item'     => __( 'Edit Tag', 'plugin-text-domain' ),
		'view_item'     => __( 'View Tag', 'plugin-text-domain' ),
		'update_item'   => __( 'Update Tag', 'plugin-text-domain' ),
		'add_new_item'  => __( 'Add New Tag', 'plugin-text-domain' ),
		'new_item_name' => __( 'New Tag', 'plugin-text-domain' ),
		'search_items'  => __( 'Search Tags', 'plugin-text-domain' ),
		'popular_items' => __( 'Popular Tags', 'plugin-text-domain' ),
		'not_found'     => __( 'No tags found', 'plugin-text-domain' ),

		// FOR NON HIERARCHY ( tags )
		'separate_items_with_commas' => __( 'Separate tags with commas', 'plugin-text-domain' ),
		'add_or_remove_items'        => __( 'Add or remove tags', 'plugin-text-domain' ),
		'choose_from_most_used'      => __( 'Choose from the most used tags', 'plugin-text-domain' ),

		// FOR HIERARCHY ( categories )
		// 'parent_item'       => __( 'Parent TAG_SINGLE_LABEL', 'plugin-text-domain' ),
		// 'parent_item_colon' => __( 'Parent TAG_SINGLE_LABEL:', 'plugin-text-domain' ), // Same as parent_item, but with a colon :

	);
	
	$rewrite = array(
		'slug'         => 'kb-tag',  // Used as pretty permalink text (i.e. /tag/) - defaults to $taxonomy (taxonomy's name slug)
		'with_front'   => true,         // allowing permalinks to be prepended with front base - defaults to true
		'hierarchical' => false,         // Boolean. Allow hierarchical urls (implemented in Version 3.1)  - default false	
	);
	
	$capabilities = array(
		// 'manage_terms' => 'manage_months',
		// 'edit_terms'   => 'edit_months',
		// 'delete_terms' => 'delete_months',
		// 'assign_terms' => 'assign_months'
	);
	
	$args = array(

		'labels'             => $labels,
		'public'             => true, // (boolean) optional ~ default true
			'publicly_queryable' => true,  // (boolean) optional ~ defaults to value of 'public'
			'show_in_nav_menus'  => true, // (boolean) optional ~ defaults to value of 'public'
			'show_ui'            => true,  // (boolean) optional ~ defaults to value of 'public'

		'show_in_menu'       => true, // (boolean) optional - here to show taxonomy in admin menu. 'show_ui' must be true ~ defaults to 'show_ui'
		'show_tagcloud'      => true, // (boolean) optional - Whether to allow the Tag Cloud widget to use this taxonomy ~ defaults to 'show_ui'
		'show_in_quick_edit' => true, // (boolean) optional ~ defaults to 'show_ui'
		
		

		
		
		// 'meta_box_cb'       => '', // (callback) optional ~ defaults to post_tags_meta_box() or post_categories_meta_box(). No metabox is shown if set to false
		'show_admin_column' => true, // (boolean) optional ~ default false
		'description'       => '', // (string) optional - Include a description of the taxonomy ~ default ''
		'hierarchical'      => false,  // (boolean) optional ~ default false
		'update_count_callback' => '_update_post_term_count', // To ensure taxonomy acts like a tag, set to '_update_post_term_count'
		'query_var'         => false, // false || string // default taxonomy name
		// 'capabilities'      => $capabilities,
		// 'sort' => true, // (boolean) optional - Whether should remember order that terms are added to objects ~ default None
		
		'rewrite'           => $rewrite,
		'delete_with_user'  => false,
		
		// 'show_in_rest'       => true, // (boolean) optional ~ default false
    	// 'rest_base'          => '*******', // (string) (optional) To change the base url of REST API route
		// 'rest_controller_class' =>


	);
	
	// register_taxonomy( 'phut_kb_tag', array( 'phut_kb' ), $args );



	// CATEGORY
	
	$labels = array(

		'name'          => _x( 'Knowledge Base Categories', 'taxonomy general name' ),
		'singular_name' => _x( 'Category', 'taxonomy singular name' ),
		'menu_name'     => _x( 'Categories', 'taxonomy general name' ),
		'all_items'     => __( 'All Categories', 'plugin-text-domain' ),
		'edit_item'     => __( 'Edit Category', 'plugin-text-domain' ),
		'view_item'     => __( 'View Category', 'plugin-text-domain' ),
		'update_item'   => __( 'Update Category', 'plugin-text-domain' ),
		'add_new_item'  => __( 'Add New Category', 'plugin-text-domain' ),
		'new_item_name' => __( 'New Category', 'plugin-text-domain' ),
		'search_items'  => __( 'Search Categories', 'plugin-text-domain' ),
		'popular_items' => __( 'Popular Categories', 'plugin-text-domain' ),
		'not_found'     => __( 'No categories found', 'plugin-text-domain' ),

		// FOR NON HIERARCHY ( tags )
		// 'separate_items_with_commas' => __( 'Separate tags with commas', 'plugin-text-domain' ),
		// 'add_or_remove_items'        => __( 'Add or remove tags', 'plugin-text-domain' ),
		// 'choose_from_most_used'      => __( 'Choose from the most used tags', 'plugin-text-domain' ),

		// FOR HIERARCHY ( categories )
		'parent_item'       => __( 'Parent Category', 'plugin-text-domain' ),
		'parent_item_colon' => __( 'Parent Category:', 'plugin-text-domain' ), // Same as parent_item, but with a colon :

	);
	
	$rewrite = array(
		'slug'         => 'kb-category',  // Used as pretty permalink text (i.e. /tag/) - defaults to $taxonomy (taxonomy's name slug)
		
		## BEGIN WHAT DOES THIS DO?
		// 'with_front'   => true,         // allowing permalinks to be prepended with front base - defaults to true
		'with_front'   => false,         // allowing permalinks to be prepended with front base - defaults to true
		## END WHAT DOES THIS DO?
		
		'hierarchical' => true,         // Boolean. Allow hierarchical urls (implemented in Version 3.1)  - default false	
	);
	
	$capabilities = array(
		// 'manage_terms' => 'manage_months',
		// 'edit_terms'   => 'edit_months',
		// 'delete_terms' => 'delete_months',
		// 'assign_terms' => 'assign_months'
	);
	
	$args = array(

		'labels'             => $labels,
		'public'             => true, // (boolean) optional ~ default true
			'publicly_queryable' => true,  // (boolean) optional ~ defaults to value of 'public'
			'show_in_nav_menus'  => true, // (boolean) optional ~ defaults to value of 'public'
			'show_ui'            => true,  // (boolean) optional ~ defaults to value of 'public'

		'show_in_menu'       => true, // (boolean) optional - here to show taxonomy in admin menu. 'show_ui' must be true ~ defaults to 'show_ui'
		'show_tagcloud'      => true, // (boolean) optional - Whether to allow the Tag Cloud widget to use this taxonomy ~ defaults to 'show_ui'
		'show_in_quick_edit' => true, // (boolean) optional ~ defaults to 'show_ui'
		
		

		
		
		// 'meta_box_cb'       => '', // (callback) optional ~ defaults to post_tags_meta_box() or post_categories_meta_box(). No metabox is shown if set to false
		'show_admin_column' => true, // (boolean) optional ~ default false
		'description'       => '', // (string) optional - Include a description of the taxonomy ~ default ''
		'hierarchical'      => true,  // (boolean) optional ~ default false
		'update_count_callback' => '_update_post_term_count', // To ensure taxonomy acts like a tag, set to '_update_post_term_count'
		'query_var'         => false, // false || string // default taxonomy name
		'query_var'         => true, // false || string // default taxonomy name
		
		// 'capabilities'      => $capabilities,
		// 'sort' => true, // (boolean) optional - Whether should remember order that terms are added to objects ~ default None
		
		'rewrite'           => $rewrite,
		'delete_with_user'  => false,
		
		// 'show_in_rest'       => true, // (boolean) optional ~ default false
    	// 'rest_base'          => '*******', // (string) (optional) To change the base url of REST API route
		// 'rest_controller_class' =>


	);
	
	register_taxonomy( 'phut_kb_category', array( 'phut_kb' ), $args );
	// register_taxonomy( 'phut_month', array( 'phut_ingredient','phut_recipe' ), $args );

	// TODO Comment out when in production
	flush_rewrite_rules( false );

} // fn

//* Register post types
function phut_kb_register_cpt() {

	$supports = array (
		'title',
		'editor',
		'excerpt',
		'author',
		// 'thumbnail',
		'custom_fields',
		'comments',
		// 'trackbacks',
		// 'revisions',
		// 'page-attributes',                  // menu order ( hierarchical post types only )
		// 'post-formats',
		// 'genesis_seo',
		'genesis-layouts',
		// 'genesis-simple-sidebars',
		'genesis-scripts',
		'genesis-cpt-archives-settings',
		// 'genesis-entry-meta-after-content',
		// 'autodescription-meta',             // The SEO Framework
		
	);

	$labels = array (
		'name'                  => 'Knowledge Base', // Seen in : Breadcrumbs, All Items list ( and, unless menu_name is set, Menu name)
		'singular_name'         => 'Article',
		// 'menu_name'             => 'CPT_PLURAL_LABEL', // Default - same as 'name'
		'name_admin_bar'        => 'Knowledge Base', // For use in New in Admin menu bar. Default is the same as `singular_name`.
		'all_items'             => 'All Articles', // String for the submenu
		'add_new'               => 'Add New',
	 	'add_new_item'          => __( 'Add a New Knowledge Base Article', 'plugin-text-domain' ),
		'edit_item'             => __( 'Edit Article', 'plugin-text-domain' ),
		'new_item'              => __( 'New Article', 'plugin-text-domain' ),
		'view_item'             => __( 'View Article', 'plugin-text-domain' ),
		'search_items'          => __( 'Search Articles', 'plugin-text-domain' ),
		'not_found'             => __( 'No Articles found', 'plugin-text-domain' ),
		'not_found_in_trash'    => __( 'No Articles found in Bin', 'plugin-text-domain' ),
		// 'parent_item_colon'     => __( 'Parent Article', 'plugin-text-domain' ), // only used in hierarchical post types
		'archives'              => __( 'Article Archives', 'plugin-text-domain' ), // for use with archives in nav menus. Default is Post Archives/Page Archives.
		'insert_into_item'      => __( 'Insert into Article', 'plugin-text-domain' ), // for the media frame button. Default is Insert into post/Insert into page.
		'uploaded_to_this_item' => __( 'Uploaded to this article', 'plugin-text-domain' ), // for the media frame filter. Default is Uploaded to this post/Uploaded to this page.
		'attributes'            => __( 'Article Attributes', 'plugin-text-domain' ), // for the attributes meta box. Default is 'Post Attributes' / 'Page Attributes'.
		'featured_image'        => __( 'Featured Image', 'plugin-text-domain' ), // Default is Featured Image.
		'set_featured_image'    => __( 'Set featured image', 'plugin-text-domain' ), // Default is Set featured image.
		'remove_featured_image' => __( 'Remove featured image', 'plugin-text-domain' ), // Default is Remove featured image.
		'use_featured_image'    => __( 'Use as featured image', 'plugin-text-domain' ), // Default is Use as featured image. 
		
	);
	
	$rewrite = array (
		'slug'       => 'knowledge-base',
		'with_front' => false, // eg: if permalink structure is /blog/, then links will be: false->/knowledgebase/, true->/blog/knowledgebase/). Defaults to true
		'feeds'      => false, // Defaults to has_archive value
		'pages'      => true, // Should the permalink structure provide for pagination. Defaults to true
	);
	
	$taxonomies = array (
		'phut_kb_category',
		// 'phut_kb_tag'
	);
	
	$args = array (
		'label'                => 'Article', // Shown in breadcrumbs, archive title, admin list table, menu items
		'labels'               => $labels,
		'description'          => __( 'CPT Description', 'plugin-text-domain' ), // (string) (optional) A short descriptive summary of what the post type is. May be visible, e.g. in menus
		'public'               => false,  // true || false - optional : Needs to be true for Yoast
			'exclude_from_search' => true, // needs to be false if want to see cpts on cpt-archive page. If need to exclude from search, then filter the search query
			'publicly_queryable'  => false,
			'show_in_nav_menus'   => false, // Preferably true if has_archive is true
			'show_ui'             => true,
		
		'show_in_menu'         => true,
		'show_in_admin_bar'    => true,
		// 'menu_position'        => null, // Defaults to below Comments
		'menu_icon'            => 'dashicons-book-alt',
		'capability_type'      => 'post', // or 'phut_kb' referenced
		'map_meta_cap'         => true,
		'hierarchical'         => false,
		'supports'             => $supports,
		
		// Called when setting up the meta boxes for the edit form.
		// Takes one argument $post, which contains the WP_Post object for the currently edited post.
		// Do remove_meta_box() and add_meta_box() calls in the callback.
		'register_meta_box_cb' => 'phut_kb_meta_box_cb',
		
		'taxonomies'           => $taxonomies,
		'has_archive'          => false,
		'rewrite'              => $rewrite,		
		'query_var'            => true, // defaults to true (custom post type name)
		'can_export'           => true, // default true
		
		// 'show_in_rest'         => true, // default false
		// 'rest_base'            => 'restbase', // The base slug that this post type will use when accessed using the REST API.
		
		/* Visibility / Archive Overide */
		'public'              => true, // Needs to be true for Yoast
		'exclude_from_search' => false, // needs to be false if want to see cpts on cpt-archive page. If need to exclude from search, then filter the search query
		'publicly_queryable'  => true,
		'has_archive'         => true,
		'show_in_nav_menus'   => true, // Preferably true if has_archive is true
		
		
	);
	
	register_post_type( 'phut_kb', $args );
	
	// Comment or remove following flush rewrite rules when not in development !!!
	flush_rewrite_rules();

}

//* Change enter title here text
function phut_kb_enter_title_here( $title ) {

	$screen = get_current_screen();
	if( $screen->post_type == 'phut_kb' )
		$title = 'Enter title here';

	return $title;
}

//* Change the post updated messages
function phut_kb_updated_messages( $messages ) {

	global $post;

	// Add messages to $messages array
	$messages['phut_kb'] = array(
		0  => '', // Unused. Messages start at index 1.
		1  => sprintf( 'Article updated. <a href="%s">View Article</a>', esc_url( get_permalink($post->ID) ) ),
		2  => 'Custom field updated.',
		3  => 'Custom field deleted.',
		4  => 'Article updated.',
		5  => isset( $_GET['revision']) ? sprintf( 'Article restored to revision from %s', wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
		6  => sprintf( 'Article published. <a href="%s">View Article</a>', esc_url( get_permalink( $post->ID ) ) ),
		7  => 'Article saved.',
		8  => sprintf( 'Article submitted. <a target="_blank" href="%s">Preview Article</a>', esc_url( add_query_arg( 'preview', 'true', get_permalink( $post->ID ) ) ) ),
		9  => sprintf( 'Article publishing scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview Article</a>', date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( get_permalink( $post->ID ) ) ),
		10 => sprintf( 'Article draft updated. <a target="_blank" href="%s">Preview Article</a>', esc_url( add_query_arg( 'preview', 'true', get_permalink( $post->ID ) ) ) ),
	);

	return $messages;
}

//* Change the bulk post updated messages
function phut_kb_bulk_updated_messages( $bulk_messages ) {

	global $bulk_counts;
	
	// Add messages to $bulk_messages array
	$bulk_messages['phut_kb'] = array(
		'updated'   => _n( '%s Article updated.', '%s Articles updated.', $bulk_counts['updated'] ),
		'locked'    => _n( '%s Article not updated, somebody is editing it.', '%s Articles not updated, somebody is editing them.', $bulk_counts['locked'] ),
		'deleted'   => _n( '%s Article permanently deleted.', '%s Articles permanently deleted.', $bulk_counts['deleted'] ),
		'trashed'   => _n( '%s Article moved to Bin.', '%s Articles moved to Bin.', $bulk_counts['trashed'] ),	
		'untrashed' => _n( '%s Article restored from Bin.', '%s Articles restored from the Bin.', $bulk_counts['untrashed'] ),
	);

	return $bulk_messages;

}

//*
function phut_kb_meta_box_cb() {

	//

}

/*
 * Use custom templates
 *
**/

add_filter('single_template', 'phut_kb_custom_templates');
add_filter('taxonomy_template', 'phut_kb_custom_templates');
add_filter('archive_template', 'phut_kb_custom_templates');

function phut_kb_custom_templates( $template ) {

	if ( is_singular('phut_kb') )
		return PHUT_KB_PATH . 'templates/single-phut_kb.php';

	if ( is_post_type_archive('phut_kb') || is_tax('phut_kb_tag') || is_tax('phut_kb_category') )
		return PHUT_KB_PATH . 'templates/archive-phut_kb.php';

	return $template;

}


//  Displays search form, if enabled TODO
function phut_kb_search() {
	// Defined in config TODO: move to plugin options
	if( !defined('PHUT_KB_ENABLE_SEARCH') || !PHUT_KB_ENABLE_SEARCH ) return;
?>
<div>
	<form role="search" method="post" id="kbsearchform" action="">
		<div class="pakb-search">
			<input type="text" value="<?php if ( is_search() ) { echo get_search_query(); } ?>" name="s" placeholder="" id="kb-s" class="autosuggest" />
			<span><input type="submit" id="searchsubmit" value="Search"/></span>
			<input type="hidden" name="post_type" value="phut_kb" />
			<?php wp_nonce_field( 'knowedge-base-search', 'search_nonce', false ); ?>
		</div>
	</form>
</div>
<?php }


// LIMIT # ARTICLES PER TAXONOMY ARCHIVE PAGE
add_filter( 'pre_get_posts', 'phut_kb_taxonomy_archives' );
function phut_kb_taxonomy_archives( $query ) {
	if ( $query->is_main_query() && !is_admin() && $query->is_tax( array( 'phut_kb_category','phut_kb_tag' ) ) ) {
		$query->set('posts_per_page', PHUT_KB_PER_PAGE);
	}
	return $query;
}